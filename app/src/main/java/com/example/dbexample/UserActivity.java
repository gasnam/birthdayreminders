package com.example.dbexample;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.niwattep.materialslidedatepicker.SlideDatePickerDialog;
import com.niwattep.materialslidedatepicker.SlideDatePickerDialogCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserActivity extends AppCompatActivity implements SlideDatePickerDialogCallback {
    Button buttonDataPicker;
    EditText nameEdit;
    TextView yearEdit;
    TextView dayEdit;
    TextView monthEdit;
    EditText surnameEdit;
    EditText patronymicEdit;
    Button saveButton;
    Button delButton;


    DataBaseHelper dataBaseHelper;
    SQLiteDatabase db;
    Cursor userCursor;
    long userId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        nameEdit = findViewById(R.id.name);
        surnameEdit = findViewById(R.id.surname);
        dayEdit = findViewById(R.id.day);
        monthEdit = findViewById(R.id.month);
        yearEdit = findViewById(R.id.year);
        patronymicEdit = findViewById(R.id.patronymic);
        buttonDataPicker = findViewById(R.id.buttonDataPicker);
        buttonDataPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar endDate = Calendar.getInstance();
                new SlideDatePickerDialog.Builder()
                        .setEndDate(endDate)
                        .setLocale(Locale.getDefault())
                        .setShowYear(false)
                        .setConfirmText("ОК")
                        .setCancelText("отмена")
                        .build()
                        .show(getSupportFragmentManager(), "TAG");
            }
        });
        saveButton = findViewById(R.id.save_button);
        delButton = findViewById(R.id.delete_button);
        dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
        }
        if (userId > 0) {
            // получаем элемент по id из бд
            userCursor = db.rawQuery("select * from " + DataBaseHelper.TABLE_NAME + " where " +
                    DataBaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});
            userCursor.moveToFirst();
            nameEdit.setText(userCursor.getString(1));
            surnameEdit.setText(userCursor.getString(2));
            dayEdit.setText(String.valueOf(userCursor.getInt(3)));
            monthEdit.setText(userCursor.getString(4));
            yearEdit.setText(String.valueOf(userCursor.getInt(5)));
            patronymicEdit.setText(userCursor.getString(6));

            userCursor.close();
        } else {
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }
    }

    public void save(View view) {
        ContentValues contentValues = new ContentValues();

        if (((validationData(nameEdit, Pattern.compile("^([A-ZА-Яa-zа-я]+-)*([A-ZА-Яa-zа-я ]+)$"), "Имя должно сотоять только из букв")
        ) && (validationData(surnameEdit, Pattern.compile("^([A-ZА-Яa-zа-я]+-)*([A-ZА-Яa-zа-я ]+)$"), "Фамилия должна сотоять только из букв")) && (validationDate(dayEdit)
        ) && (validatoinPatronymic(patronymicEdit, Pattern.compile("^([A-ZА-Яa-zа-я]+-)*([A-ZА-Яa-zа-я ]+)$"), "Отчество должно сотоять только из букв"))
        )
        ) {

            contentValues.put(DataBaseHelper.COLUMN_NAME, nameEdit.getText().toString());
            contentValues.put(DataBaseHelper.COLUMN_SURNAME, surnameEdit.getText().toString());
            contentValues.put(DataBaseHelper.COLUMN_DAY, Integer.parseInt(dayEdit.getText().toString()));
            contentValues.put(DataBaseHelper.COLUMN_MONTH, monthEdit.getText().toString());
            contentValues.put(DataBaseHelper.COLUMN_YEAR, Integer.parseInt(yearEdit.getText().toString()));
            contentValues.put(DataBaseHelper.COLUMN_PATRONYMIC, patronymicEdit.getText().toString());

            if (userId > 0) {
                db.update(DataBaseHelper.TABLE_NAME, contentValues, DataBaseHelper.COLUMN_ID + "=" + String.valueOf(userId), null);
            } else {
                db.insert(DataBaseHelper.TABLE_NAME, null, contentValues);
            }
            goHome();
        }
    }

    public void delete(View view) {
        db.delete(DataBaseHelper.TABLE_NAME, "_id=?", new String[]{String.valueOf(userId)});
        goHome();
    }

    private void goHome() {
        db.close();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public void onPositiveClick(int day, int month, int year, Calendar calendar) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MMMM.yyyy", Locale.getDefault());

        String[] str = format.format(calendar.getTime()).split("[.]");
        dayEdit.setText(str[0]);
        monthEdit.setText(str[1]);
        yearEdit.setText(str[2]);
    }

    private boolean validationData(EditText editText, Pattern pattern, String string) {
        if (!editText.getText().toString().isEmpty()) {
            String str = editText.getText().toString();
            Matcher matcher = pattern.matcher(str);
            if (!matcher.find()) {
                editText.setError(string);
                return false;
            }
            return true;
        }
        editText.setError("Поле должно быть запоненно");
        return false;
    }

    private boolean validationDate(TextView textView) {
        if (!textView.getText().toString().isEmpty()) {
            return true;
        }
        Toast.makeText(getApplicationContext(), "Введите дату нажав на кнопку", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean validatoinPatronymic(EditText editText, Pattern pattern, String string) {
        if (!editText.getText().toString().isEmpty()) {
            String str = editText.getText().toString();
            Matcher matcher = pattern.matcher(str);
            if (!matcher.find()) {
                editText.setError(string);
                return false;
            }
            return true;
        }
        return true;
    }

}
