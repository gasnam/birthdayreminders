package com.example.dbexample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "birthdayreminders.db";
    private static final int DATABASE_VERSION = 1;

    static final String TABLE_NAME = "usersInformation";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_SURNAME = "surname";
    static final String COLUMN_DAY = "day";
    static final String COLUMN_MONTH = "month";
    static final String COLUMN_YEAR = "age";
    static final String COLUMN_PATRONYMIC = "patronymic";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_SURNAME + " TEXT," +
                    COLUMN_DAY + " INTEGER," +
                    COLUMN_MONTH + " TEXT," +
                    COLUMN_YEAR + " INTEGER," +
                    COLUMN_PATRONYMIC + " TEXT);";

    private static final String SQL_DELETE_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //создание таблицы
        db.execSQL(SQL_CREATE_TABLE);
        // добавление начальных данных


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);

    }
}
